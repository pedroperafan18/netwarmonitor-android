package com.perafan.netwarmonitor.netwarmonitor.models;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by perafan on 8/07/17.
 */

public class Caminata  extends RealmObject {
    public Date fechaInicio;
    public Date fechaFinal;
    public RealmList<GeoPosiciones> posiciones;
}
