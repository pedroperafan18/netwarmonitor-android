package com.perafan.netwarmonitor.netwarmonitor.activities;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.perafan.netwarmonitor.netwarmonitor.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.perafan.netwarmonitor.netwarmonitor.adapter.CaminataAdapter.jsonArrayPosicionesMapa;

public class MapaCaminataActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_caminata);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        PolylineOptions lineOptions = null;

        JSONArray jsonArrayPosiciones = jsonArrayPosicionesMapa;
        double iniciolat = 0;
        double iniciolng = 0;
        ArrayList<LatLng> points = new ArrayList();
        for (int i=0;i<jsonArrayPosiciones.length();i++){
            lineOptions = new PolylineOptions();
            try {
                JSONObject jsonObject = jsonArrayPosiciones.getJSONObject(i);
                double lat = Double.parseDouble(jsonObject.getString("latitud"));
                double lng = Double.parseDouble(jsonObject.getString("longitude"));
                if(iniciolat == 0){
                    iniciolat = lat;
                    iniciolng = lng;
                }
                LatLng position = new LatLng(lat, lng);
                points.add(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        //LatLng position = new LatLng(20.6700005,-103.3540745);
        //points.add(position);
        lineOptions.addAll(points);
        lineOptions.width(12);
        lineOptions.color(Color.RED);
        lineOptions.geodesic(true);

        LatLng inicio = new LatLng(iniciolat, iniciolng);
        mMap.addMarker(new MarkerOptions()
                .position(inicio));

        CameraPosition cameraPosition = CameraPosition.builder()
                .target(inicio)
                .zoom(17)
                .build();

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.addPolyline(lineOptions);
    }
}
