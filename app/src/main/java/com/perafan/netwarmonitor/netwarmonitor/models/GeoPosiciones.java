package com.perafan.netwarmonitor.netwarmonitor.models;

import io.realm.RealmObject;

/**
 * Created by perafan on 8/07/17.
 */

public class GeoPosiciones extends RealmObject {

    public double latitude;
    public double longitude;

}
