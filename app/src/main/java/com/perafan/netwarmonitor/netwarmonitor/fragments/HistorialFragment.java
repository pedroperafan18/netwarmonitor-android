package com.perafan.netwarmonitor.netwarmonitor.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.perafan.netwarmonitor.netwarmonitor.adapter.CaminataAdapter;
import com.perafan.netwarmonitor.netwarmonitor.adapter.CaminataDataSource;
import com.perafan.netwarmonitor.netwarmonitor.R;
import com.perafan.netwarmonitor.netwarmonitor.models.Caminata;
import com.perafan.netwarmonitor.netwarmonitor.models.GeoPosiciones;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

public class HistorialFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public HistorialFragment() {

    }
    // TODO: Rename and change types and number of parameters
    public static HistorialFragment newInstance() {
        HistorialFragment fragment = new HistorialFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private RecyclerView recyclerHistorial;
    private CaminataAdapter caminataAdapter;

    private SwipeRefreshLayout swipeRefresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_historial, container, false);
        swipeRefresh = (SwipeRefreshLayout) vista.findViewById(R.id.swipeRefresh);
        recyclerHistorial = (RecyclerView) vista.findViewById(R.id.recyclerHistorial);

        caminataAdapter = new CaminataAdapter(getActivity(),getHistorial());
        recyclerHistorial.setHasFixedSize(true);
        recyclerHistorial.setAdapter(caminataAdapter);
        recyclerHistorial.setLayoutManager(new LinearLayoutManager(getActivity().getBaseContext()));

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshHistorial();
            }
        });
        return vista;
    }

    // Refreshing all the RecyclerView items, the families
    private void refreshHistorial(){
        // Load items
        caminataAdapter = new CaminataAdapter(getActivity(),getHistorial());
        // Load complete
        onFamiliesLoadComplete();
    }

    // Updating and showing the family's adapter
    private void onFamiliesLoadComplete() {
        // Update the adapter and notify data set changed
        recyclerHistorial.setAdapter(caminataAdapter);
        recyclerHistorial.setLayoutManager(new LinearLayoutManager(getActivity().getBaseContext()));
        // Stop refresh animation
        swipeRefresh.setRefreshing(false);
    }

    public List<CaminataDataSource> getHistorial(){
        List<CaminataDataSource> caminataDataSourceList = new ArrayList<>();

        Realm.init(getActivity().getBaseContext());
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Caminata> caminatas = realm.where(Caminata.class).findAllSorted("fechaInicio", Sort.DESCENDING);
        for(int i=0;i<caminatas.size();i++){
            CaminataDataSource caminataDataSource = new CaminataDataSource();
            caminataDataSource.fechaInicio = String.valueOf(caminatas.get(i).fechaInicio);
            caminataDataSource.fechaFinal = String.valueOf(caminatas.get(i).fechaFinal);
            caminataDataSourceList.add(caminataDataSource);
            RealmList<GeoPosiciones> posiciones = caminatas.get(i).posiciones;
            int numeroPosiciones = posiciones.size();
            caminataDataSource.posiciones = new JSONArray();
            for (int j=0;j<numeroPosiciones;j++){
                JSONObject jsonObjectPosicion = new JSONObject();
                try {
                    jsonObjectPosicion.put("latitud",posiciones.get(j).latitude);
                    jsonObjectPosicion.put("longitude",posiciones.get(j).longitude);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                caminataDataSource.posiciones.put(jsonObjectPosicion);
            }
        }
        /*
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            //Obtiene el id del usuario
            String uid = user.getUid();
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = database.getReference(uid+"/caminatas");

            // Attach a listener to read the data at our posts reference
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot child:dataSnapshot.getChildren()) {
                        CaminataDataSource caminataDataSource = new CaminataDataSource();
                        HashMap objeto = (HashMap) child.getValue();
                        caminataDataSource.fechaInicio = String.valueOf(objeto.get("Inicio"));
                        caminataDataSource.fechaFinal = String.valueOf(objeto.get("Final"));
                        //caminataDataSource.posiciones = (JSONArray) objeto.get("Posiciones");
                        ArrayList list = (ArrayList) objeto.get("Posiciones");
                        caminataDataSource.posiciones = new JSONArray(list);
                        caminataDataSourceList.add(caminataDataSource);
                        Object inicioCaminata = objeto.get("Inicio");
                        Object finalCaminata = objeto.get("Final");
                        Object posicionesCaminata = objeto.get("Posiciones");

                        System.out.println(inicioCaminata);
                        System.out.println(finalCaminata);
                        System.out.println(posicionesCaminata);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        }
        */
        return caminataDataSourceList;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
