package com.perafan.netwarmonitor.netwarmonitor.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.SupportMapFragment;

public class MapaFragment extends SupportMapFragment {

    public MapaFragment() {
    }

    public static MapaFragment newInstance() {
        return new MapaFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);

        return root;

    }

}
