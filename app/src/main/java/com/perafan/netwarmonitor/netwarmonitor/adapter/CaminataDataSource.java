package com.perafan.netwarmonitor.netwarmonitor.adapter;

import org.json.JSONArray;

import java.io.Serializable;

/**
 * Created by perafan on 9/07/17.
 */

public class CaminataDataSource implements Serializable {
    public String fechaInicio;
    public String fechaFinal;
    public JSONArray posiciones;
}
