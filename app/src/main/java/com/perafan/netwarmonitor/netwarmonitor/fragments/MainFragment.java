package com.perafan.netwarmonitor.netwarmonitor.fragments;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.perafan.netwarmonitor.netwarmonitor.R;
import com.perafan.netwarmonitor.netwarmonitor.adapter.CaminataDataSource;
import com.perafan.netwarmonitor.netwarmonitor.models.Caminata;
import com.perafan.netwarmonitor.netwarmonitor.models.GeoPosiciones;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;


public class MainFragment extends Fragment implements OnMapReadyCallback,GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks{
    private OnFragmentInteractionListener mListener;


    private double latitud = 0;
    private double longitud = 0;
    private MapaFragment MapaFragment;
    private Button btnCaminata;
    private Caminata caminata;
    private ComenzarCaminata comenzarCaminata;
    private GoogleApiClient apiClient;
    private static final String LOGTAG = "android-localizacion";
    private static final int PETICION_PERMISO_LOCALIZACION = 101;

    public MainFragment() {
        // Required empty public constructor
    }
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_main, container, false);

        apiClient = new GoogleApiClient.Builder(getActivity().getBaseContext())
                .enableAutoManage(getActivity(), this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();

        btnCaminata = (Button) root.findViewById(R.id.btnCaminata);
        btnCaminata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //cuando se le da click al boton se crea un objeto caminata que es e que almacena los datos
                if(comenzarCaminata == null){
                    caminata = new Caminata();
                    caminata.fechaInicio = new Date();
                    //Guarda las posiciones en un objeto de realm
                    caminata.posiciones = new RealmList<GeoPosiciones>();
                    comenzarCaminata = new ComenzarCaminata();
                    comenzarCaminata.execute();
                }else{
                    caminata.fechaFinal = new Date();
                    //Guarda en realm
                    guardarEnLocal();
                    //Guardar en firebase
                    guardarEnFirebase();
                    comenzarCaminata.cancel(true);
                    comenzarCaminata = null;
                }
            }
        });

        MapaFragment = MapaFragment.newInstance();
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.map, MapaFragment)
                .commit();
        return root;
    }

    public void guardarEnLocal(){
        Realm.init(getActivity().getBaseContext());
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealm(caminata);
        realm.commitTransaction();
    }

    public void guardarEnFirebase(){

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {

            //Obtiene el id del usuario
            String uid = user.getUid();

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference databaseReferenceUsuario = database.getReference(uid);
            List lista = new ArrayList();
            //se crea una lista con todas las posiciones
            for(int i=0;i<caminata.posiciones.size();i++){
                objectoFireBase objectoFireBase = new objectoFireBase();
                objectoFireBase.put("Latitud",caminata.posiciones.get(i).latitude);
                objectoFireBase.put("Longitud",caminata.posiciones.get(i).longitude);
                lista.add(objectoFireBase);
            }

            String key = databaseReferenceUsuario.child("caminatas").push().getKey();
            objectoFireBase objectoFireBaseFnicio = new objectoFireBase();
            objectoFireBaseFnicio.put("Inicio",caminata.fechaInicio);
            objectoFireBaseFnicio.put("Final",caminata.fechaFinal);
            objectoFireBaseFnicio.put("Posiciones",lista);

            //agrega una posiciones nueva en firebase
            databaseReferenceUsuario.child("caminatas").child(key).setValue(objectoFireBaseFnicio);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        apiClient.stopAutoManage(getActivity());
        apiClient.disconnect();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        //Se coloca en la posicion actual
        LatLng ubicacion = new LatLng(latitud, longitud);
        googleMap.addMarker(new MarkerOptions()
                .position(ubicacion));

        CameraPosition cameraPosition = CameraPosition.builder()
                .target(ubicacion)
                .zoom(17)
                .build();

        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        //Se ha producido un error que no se puede resolver automáticamente
        //y la conexión con los Google Play Services no se ha establecido.

        Log.e(LOGTAG, "Error grave al conectar con Google Play Services");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //Conectado correctamente a Google Play Services
        try{
            if (ActivityCompat.checkSelfPermission(getActivity().getBaseContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        PETICION_PERMISO_LOCALIZACION);
            } else {

                Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);

                updateUI(lastLocation);
            }
        }catch (NullPointerException e){
            Log.e("NullPointerException",e.getMessage());
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        //Se ha interrumpido la conexión con Google Play Services

        Log.e(LOGTAG, "Se ha interrumpido la conexión con Google Play Services");
    }

    private void updateUI(Location loc) {
        if (loc != null) {
            longitud =  loc.getLongitude();
            latitud = loc.getLatitude();

            if(caminata != null){
                GeoPosiciones geoPosiciones = new GeoPosiciones();
                geoPosiciones.latitude = latitud;
                geoPosiciones.longitude = longitud;
                caminata.posiciones.add(geoPosiciones);
            }
            MapaFragment.getMapAsync(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PETICION_PERMISO_LOCALIZACION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //Permiso concedido

                @SuppressWarnings("MissingPermission")
                Location lastLocation =
                        LocationServices.FusedLocationApi.getLastLocation(apiClient);

                updateUI(lastLocation);

            } else {
                //Permiso denegado:
                //Deberíamos deshabilitar toda la funcionalidad relativa a la localización.

                Log.e(LOGTAG, "Permiso denegado");
            }
        }
    }

    //Clase de Asynctask para crear el hilo que obternga la posicion cada segundo
    private class ComenzarCaminata extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {

            try{
                while(true) {
                    //Corre en el hilo principal
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            onConnected(Bundle.EMPTY);
                        }
                    });
                    //1000 milisegundos
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    //Clase del objeto que se usa para guardar en firebase
    class objectoFireBase extends HashMap{

    }

}
