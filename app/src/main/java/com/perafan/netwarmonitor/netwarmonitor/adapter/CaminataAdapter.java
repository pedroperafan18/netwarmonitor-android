package com.perafan.netwarmonitor.netwarmonitor.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.perafan.netwarmonitor.netwarmonitor.R;
import com.perafan.netwarmonitor.netwarmonitor.activities.MapaCaminataActivity;

import org.json.JSONArray;

import java.util.Collections;
import java.util.List;

/**
 * Created by perafan on 9/07/17.
 */

public class CaminataAdapter extends RecyclerView.Adapter<CaminataAdapter.CaminataViewHolder> implements Filterable {
    public static JSONArray jsonArrayPosicionesMapa;

    private Activity adapterActivity;
    private LayoutInflater inflater;
    private List<CaminataDataSource> items = Collections.emptyList();

    public CaminataAdapter(Activity activity, List<CaminataDataSource> data) {
        inflater = LayoutInflater.from(activity);
        items = data;
        adapterActivity = activity;
    }

    @Override
    public CaminataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("onCreateViewHolder","onCreateViewHolder");
        View view = inflater.inflate(R.layout.item_caminata, parent, false);
        CaminataViewHolder holder = new CaminataViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CaminataViewHolder holder, int position) {
        Log.e("onBindViewHolder","onBindViewHolder");
        CaminataDataSource caminataDataSource = items.get(position);
        holder.textFechaInicial.setText(caminataDataSource.fechaInicio);
        holder.textFechaFinal.setText(caminataDataSource.fechaFinal);
        holder.jsonArrayposiciones = caminataDataSource.posiciones;
    }

    @Override
    public int getItemCount() {
        Log.e("getItemCount","getItemCount");
        if (items != null){
            Log.e("return", String.valueOf(items.size()));
            return items.size();
        }

        return 0;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    class CaminataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView textFechaInicial;
        TextView textFechaFinal;
        JSONArray jsonArrayposiciones;
        public CaminataViewHolder(View itemView) {
            super(itemView);
            textFechaInicial = (TextView) itemView.findViewById(R.id.textViewFechaInicial);
            textFechaFinal = (TextView) itemView.findViewById(R.id.textViewFechaFinal);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            jsonArrayPosicionesMapa = jsonArrayposiciones;
            Intent intent = new Intent(adapterActivity, MapaCaminataActivity.class);
            adapterActivity.startActivity(intent);
        }
    }

}
